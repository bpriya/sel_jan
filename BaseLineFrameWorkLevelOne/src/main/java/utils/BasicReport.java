package utils;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {

		ExtentHtmlReporter html;
		ExtentReports extent;
		ExtentTest test;	
		@Test
		public void runReport() {
			
				try {
					html= new ExtentHtmlReporter("./report/extentreport.html");																																																																																																																																															extent=new ExtentReports();
					html.setAppendExisting(true);
					extent=new ExtentReports();
					extent.attachReporter(html);
					test = extent.createTest("TC_001_Login", "Login to Leaftaps");
					test.assignAuthor("Banu");
					test.assignCategory("Smoke");
					test.pass("username entered successfully");
					test.fail("failed", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
				} 
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				extent.flush();
		}	

	}


