package utils;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	static Object[][] data;

	public static Object[][] readdata(String dataSheetName) {
		// TODO Auto-generated method stub
		
		
		XSSFWorkbook wbook;
		try {
			wbook = new XSSFWorkbook("./data"+dataSheetName+".xlsx");
		
		
		XSSFSheet sheet=wbook.getSheet("Sheet1");
		
		//getting last row count- by default poi excludes header row
		int rowCount= sheet.getLastRowNum();
		
		//getting last column name for header row
		int colCount= sheet.getRow(0).getLastCellNum();
		
		data= new Object[rowCount][colCount];
		
		for(int i=1; i<=rowCount;i++)
		{
			XSSFRow row=sheet.getRow(i);
			
			for(int j=0;j<colCount;j++)
			{
				XSSFCell cell = row.getCell(j);
				CellType cellType = cell.getCellType();
				//System.out.println(cellType);
				data[i-1][j] = cell.getStringCellValue();
				//System.out.println("The data in row "+i+ "and column " +j+ "is "+text);
			}
			
		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return data;
	}

}
