package bl.framework.design;

import org.junit.BeforeClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;

public class ProjectMethods extends SeleniumBase {
	
	public String TCname,des,author,category,dataSheetName;
	
	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}
	
	@AfterSuite
	public void afterSuite() {
		endreport();
	}
	
	@BeforeClass
	public void beforeclass() {
		initializetest(TCname,des,author,category);
	}
	
	@BeforeMethod
	public void login()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		clearAndType(locateElement("id", "username"), "DemoSalesManager"); 
		clearAndType(locateElement("id", "password"), "crmsfa"); 
		click(locateElement("class", "decorativeSubmit"));
		click(locateElement("linkText","CRM/SFA"));
		
	}
	
	@DataProvider(name="getData")
	public Object[][] getdata(){
		return ReadExcel.readdata(dataSheetName);
		
	}
	
}
