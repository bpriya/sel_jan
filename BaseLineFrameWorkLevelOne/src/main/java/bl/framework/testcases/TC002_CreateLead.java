package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
	TCname="TC002_Create Lead";
	des="Create lead";
	author="banu";
	category="Smoke";
	dataSheetName="TC001";
	}
	
	@Test(invocationCount=2,invocationTimeOut=30000)
	public void createLead() {
		click(locateElement("linkText","Create Lead"));
		WebElement eleCompname = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompname, "IBM");
		clearAndType(locateElement("id", "createLeadForm_firstName"),"Banu");
		clearAndType(locateElement("id", "createLeadForm_lastName"),"Priya");
		WebElement eleCreateSubmit = locateElement("name","submitButton");
		click(eleCreateSubmit);
		close();
		
	}

}
