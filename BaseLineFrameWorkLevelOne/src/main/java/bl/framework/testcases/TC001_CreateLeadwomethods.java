package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_CreateLeadwomethods extends SeleniumBase{

	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		/*WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); */
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").clear();
		driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
		driver.findElementById("createLeadForm_firstName").sendKeys("Banu");
		driver.findElementById("createLeadForm_lastName").sendKeys("Priya");
		driver.findElementByName("submitButton").click();
	}
}








