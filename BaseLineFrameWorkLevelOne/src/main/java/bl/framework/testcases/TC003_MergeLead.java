package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods {
	
	@Test
	public void MergeLead(){
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Merge Leads"));
		clickwithoutSnap(locateElement("xpath","(//img[@alt='Lookup'])[1]"));
		switchToWindow("Find Leads");
		clearAndType(locateElement("name", "firstName"), "Banu");
		clearAndType(locateElement("name", "lastName"), "Priya");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Explicitwait(locateElement("xpath","//div[text()='Lead ID']"),10);
		clickwithoutSnap(locateElement("xpath","(//td[contains(@class,'cell-first')]//a)[1]"));
		switchToWindow(0);
		clickwithoutSnap(locateElement("xpath","(//img[@alt='Lookup'])[2]"));
		switchToWindow("Find Leads");
		clearAndType(locateElement("name", "firstName"), "Banu");
		clearAndType(locateElement("name", "lastName"), "Priya");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Explicitwait(locateElement("xpath","//div[text()='Lead ID']"),10);
		clickwithoutSnap(locateElement("xpath","(//td[contains(@class,'cell-first')]//a)[2]"));
		switchToWindow(0);
		close();
		/*click(locateElement("linkText","Merge"));
		//switchToAlert();
		acceptAlert();
		takeSnap();
		close();*/
		
	}

}
