package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.Advancereport;

public class SeleniumBase extends Advancereport implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		logStep("chrome bowser opened","pass");
		//System.out.println("The chrome browser is launched by defaut and URL"+url+"is entered");
        //takeSnap();

	}
	
	
	public void Explicitwait(WebElement ele, int time) {
		WebDriverWait wait= new WebDriverWait(driver,time);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
	}
	

	@Override
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver(); 
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully");
        takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
		switch (locatorType) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "linkText":return driver.findElementByLinkText(value);
		case "partialtext": return driver.findElementByPartialLinkText(value);
		case "tag": return driver.findElementByTagName(value);
		default:
			break;
		}
		}
		catch(NoSuchElementException e) {
			System.err.println("Element is not present");
		}
		catch(NotFoundException e) {
		}
		catch(WebDriverException e)
		{
		}
		return null;
	}
		

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		try {
		return driver.findElementById(value);
		}
		catch(NoSuchElementException e) {
			System.err.println("Element is not present");
		}
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		switch (type) {
		case "id": return driver.findElementsById(value);
		case "name": return driver.findElementsByName(value);
		case "class": return driver.findElementsByClassName(value);
		case "xpath": return driver.findElementsByXPath(value);
		case "linkText":return driver.findElementsByLinkText(value);
		case "partialtext": return driver.findElementsByPartialLinkText(value);
		case "tag": return driver.findElementsByTagName(value);
		default:
			break;
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().alert();
		System.out.println("Switched to alert");
		}
		catch(NoAlertPresentException e) {
		System.err.println("No alert presnet");	
		e.printStackTrace();
		}
		catch(NotFoundException e) {
		}
		catch(WebDriverException e)
		{
		}
		

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().alert().accept();
		}
		catch(NoAlertPresentException e) {
			System.err.println("No alert presnet");	
			e.printStackTrace();
			}
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
		}
		catch(NoAlertPresentException e) {
			System.err.println("No alert presnet");	
			e.printStackTrace();
			}
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		try {
		String text = driver.switchTo().alert().getText();
		return text;
		}
		catch(NoAlertPresentException e) {
			System.err.println("No alert presnet");	
			e.printStackTrace();
			}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().alert().sendKeys(data);
		System.out.println(data+"is entered in Alert box");
		}
		catch(NoAlertPresentException e) {
			System.err.println("No alert presnet");	
			e.printStackTrace();
			}

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listhandles=new ArrayList<>(windowHandles);
		
		driver.switchTo().window(listhandles.get(index));
		}
		catch(NoSuchWindowException e) {
			System.err.println("No such window presnet");	
			e.printStackTrace();
			}
		catch(NotFoundException e) {
			
		}
		catch(WebDriverException e)
		{
		}	

	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		try {
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> listhandles=new ArrayList<>(windowHandles);
		
		for (String wins : listhandles) {
			if(driver.switchTo().window(wins).equals(title))
				break;
		}
		}
		catch(NoSuchWindowException e) {
			System.err.println("No such window presnet");	
			e.printStackTrace();
			}
	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().frame(index);
		takeSnap();
		}
		catch(NoSuchFrameException e) {
			System.err.println("No such frame presnet");	
			e.printStackTrace();	
		}
		catch(NotFoundException e)
		{
		}
		catch(WebDriverException e)
		{
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().frame(ele);
		takeSnap();
		}
		catch(NoSuchFrameException e) {
			System.err.println("No such frame presnet");	
			e.printStackTrace();	
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().frame(idOrName);
		takeSnap();
		}
		catch(NoSuchFrameException e) {
			System.err.println("No such frame presnet");	
			e.printStackTrace();	
		}
	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub
		driver.switchTo().defaultContent();
		takeSnap();

	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		if(driver.getCurrentUrl().equals(url))
			return true;
		else
			return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		if(driver.getTitle().equals(title))
			return true;
		else
			return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		try {
		driver.close();
		}
		catch(WebDriverException e) {
			
		}

	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		try {
		driver.quit();
		}
		catch(WebDriverException e) {
			
		}

	}

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		takeSnap();
		}
		catch(StaleElementReferenceException e) {
			e.printStackTrace();
		}
		catch(WebDriverException e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void clickwithoutSnap(WebElement ele) {
		try {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		}
		catch(StaleElementReferenceException e) {
			e.printStackTrace();
		}
		catch(WebDriverException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		try {
		ele.sendKeys(data);
		}
		catch(ElementNotInteractableException e)
		{
			e.printStackTrace();
		}
		catch(InvalidElementStateException e)
		{
			
		}
		
	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		try {
		ele.clear();		
		}
		catch(InvalidElementStateException e)
		{
			
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
		ele.clear();
		ele.sendKeys(data); 
		System.out.println("The data "+data+" entered successfully");
		takeSnap();
		}
		
		catch(ElementNotInteractableException e)
		{
			e.printStackTrace();
		}
		catch(InvalidElementStateException e)
		{
			
		}
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		try {
		String txt= ele.getText();
		return txt;
		}
		catch(NoSuchElementException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		try {
		String bccolor=ele.getCssValue("background-color");
		return bccolor;
		}
		catch(NoSuchElementException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return ele.getText();
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
		Select sel= new Select(ele);
		sel.selectByVisibleText(value);
		takeSnap();
		}
		catch(NoSuchElementException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
		Select sel= new Select(ele);
		sel.selectByIndex(index);
		takeSnap();
		}catch(NoSuchElementException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
		Select sel= new Select(ele);
		sel.selectByValue(value);
		takeSnap();
		}
		catch(NoSuchElementException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		if(ele.getText().equals(expectedText))
		{
			System.out.println(ele+" text is equal to the "+expectedText);
			return true;
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		if(ele.getText().contains(expectedText))
		{
			System.out.println(ele+" text is equal to the "+expectedText);
			return true;
		}
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		if(ele.getCssValue(attribute).equals(value))
			return true;
		return false;
	}

	@Override
	public boolean verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		if(ele.getCssValue(attribute).contains(value))
			return true;
		return false;
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isDisplayed())
			return true;
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		if(!ele.isDisplayed())
			return true;
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isEnabled())
			return true;
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isEnabled())
			return true;
		return false;
	}

}
